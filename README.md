Jupyter Notebooks for the Deep Learning course at EURECOM.

Lab 1 :
Implementation, training and testing a Neural Network for the Handwritten Digits Recognition problem [1] with different settings of hyperparameters. The MNIST dataset is used,  constructed from scanned documents available from the National Institute of Standards and Technology (NIST).

Lab 2 :
Building, training and optimization in TensorFlow of one of the early Convolutional Neural Networks, LeNet-5.